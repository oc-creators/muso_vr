﻿using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine;
//using 
using System.Collections;
using Enemy;

namespace General
{

    public class GameManager : ScreenManager<GameManager>
    {
        [SerializeField][Range(0f, 5f)] private float waitTime = 1f;

        [SerializeField] private GameObject enemy;

        [SerializeField] private SpawnerPoints sp;

        [SerializeField] [Range(1, 100)] private int maxEnemyNum = 100;

        // private int enemyNum = 0;

        // private int downedEnemyNum = 0;

        [SerializeField] private AudioClip countDownAC;

        [SerializeField] private AudioClip bgmClip;

        [SerializeField] private Transform enemyGroup;

        private int minutes;

        private int seconds;

        [SerializeField] Text timerText;

        [SerializeField] private float interval = 1f;

        protected override void Start()
        {
            base.Start();

            if (gfc.VMode != ViewMode.GameEntry)
            {
                gfc.VMode = ViewMode.GameEntry;
            }
            if (gfc.SMode != ScreenMode.Game)
            {
                gfc.SMode = ScreenMode.Game;
            }

            gfc.Views = views;

            InitGame();
        }

        private void OnEnable()
        {

        }

        protected override void Update()
        {
            

            if(pb.DownedEnemyNum >= pb.LimitEnemyNum)
            {
                EndGame();
            }

            if (pb.IsStart && !pb.IsOver) pb.Elapsed += Time.deltaTime;

            if (pb.IsOver)
            {
                if (OVRInput.GetDown(OVRInput.RawButton.LIndexTrigger))
                {
                    am.Stop();
                    gfc.dispatch(Signal.ToTitle);
                }
            }
        }

        public void InitGame()
        {
            am.Play(bgmClip);
        }

        /*
        public IEnumerator PlayTimer()
        {

        }
        */

        public void StartGame()
        {
            if (pb.IsStart) return;

            pb.IsStart = true;

            gfc.dispatch(Signal.Forward);

            StartCoroutine(CountDown());
        }

        public IEnumerator CountDown()
        {
            WaitForSeconds waitSec = new WaitForSeconds(waitTime);
            yield return waitSec;
            /*
            // カウントダウンの音声ファイルの数だけカウント
            for (int i = countdownSe.Length - 1; i >= 0; i--)
            {
                if (i == 0)
                {
                    countdownText.text = goText;
                }
                else
                {
                    countdownText.text = (i).ToString();
                }
                if (countdownSe[i] != null)
                {
                    soundManager.PlaySe1(countdownSe[i]);
                    //soundManager.PlaySe(i - 1);
                    //audiosource.PlayOneShot(countdownSe[i]);
                }
                yield return waitOneSec;
            }
            */

            am.PlaySE(countDownAC);

            for(int i = 3; i >= 0; i--)
            {
                Debug.Log("CountDownTime: " + i);
                yield return waitSec;
            }

            // 敵を呼び出す
            /*
            for(int i = 0; i < pb.InitEnemyNum; i++)
            {
                SummonEnemy();
            }
            */
            SummonEnemy();
            
        }

        public void SummonEnemy()
        {
            if (pb.DownedEnemyNum % 20 != 0) return;

            if(pb.EnemyNum < pb.LimitEnemyNum)
            {
                // 発生ポイント決定(順番で)
                // Transform point;
                // point = sp.points[pb.EnemyNum % sp.points.Length];

                // 敵を生成
                for(int i = 0; i < sp.points.Length; i++)
                {
                    Transform point;
                    point = sp.points[i];
                    for(int j = 0; j < 4; j++)
                    {
                        GameObject newNPC = Instantiate(enemy, point.position + new Vector3(0, 0, interval * j), Quaternion.identity);
                        // newNPC.transform.parent = transform.GetChild(0).transform;
                        newNPC.transform.parent = enemyGroup;
                        newNPC.SetActive(true);
                        pb.EnemyNum++;
                    }
                }
            }
        }

        public void EndGame()
        {
            if(pb.IsOver)
            {
                return;
            }

            if (pb.IsClear)
            {

            }

            pb.IsOver = true;

            minutes = (int)pb.Elapsed / 60;
            seconds = (int)(pb.Elapsed - minutes * 60);

            timerText.text = minutes.ToString("D2") + ":" + ((int)seconds).ToString("D2");

            gfc.dispatch(Signal.Forward);
        }

    }

}