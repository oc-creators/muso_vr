﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace General
{
    public class InputManager : SingletonMonoBehaviour<InputManager>
    {
        protected override bool dontDestroyOnLoad { get { return false; } }

        protected override void Init()
        {

        }

        private void OnEnable()
        {

        }

        private void OnDisable()
        {

        }

        protected override void Update()
        {

        }
    }
}
