﻿using UnityEngine;
using UnityEngine.UI;

namespace General
{
    public class ParamBridge : SingletonMonoBehaviour<ParamBridge>
    {
        protected override bool dontDestroyOnLoad { get { return true; } }

        // ==================================
        // 固定値
        // ==================================

        // 制限時間初期値上限値
        public const float INIT_ELAPSED = 0f;
        public const float LIMIT_ELAPSED = 600f;

        // ==================================
        // パラメータ変数
        // ==================================

        // 時を止めたかどうか
        [SerializeField] private bool stopTheWorld = false;
        public bool StopTheWorld
        {
            get { return stopTheWorld; }
            set
            {
                stopTheWorld = value;
                if (value)
                {
                    Time.timeScale = 1f;
                }
            }
        }
        // 経過時間
        [SerializeField] private float elapsed = INIT_ELAPSED;
        public float Elapsed
        {
            get { return elapsed; }
            set { elapsed = value; }
        }
        // ゲームが始まったかどうか
        [SerializeField] private bool isStart = false;
        public bool IsStart
        {
            get { return isStart; }
            set { isStart = value; }
        }
        // ゲーム終了したかどうか
        [SerializeField] private bool isOver = false;
        public bool IsOver
        {
            get { return isOver; }
            set { isOver = value; }
        }
        // ゲームをクリアしたかどうか
        [SerializeField] private bool isClear = false;
        public bool IsClear
        {
            get { return isClear; }
            set { isClear = value; }
        }
        [SerializeField] private Transform playerTR;
        public Transform PlayerTR
        {
            get { return playerTR; }
            set { playerTR = value; }
        }
        // 出現した敵の総数
        [SerializeField] private int enemyNum = 0;
        public int EnemyNum
        {
            get { return enemyNum; }
            set { enemyNum = value; }
        }
        // 倒された敵の数
        [SerializeField] private int downedEnemyNum = 0;
        public int DownedEnemyNum
        {
            get { return downedEnemyNum; }
            set { downedEnemyNum = value; }
        }
        // スタート時の敵の数
        [SerializeField] private int initEnemyNum = 1;
        public int InitEnemyNum
        {
            get { return initEnemyNum; }
            set { initEnemyNum = value; }
        }
        // 終了時の敵の数
        [SerializeField] private int limitEnemyNum = 100;
        public int LimitEnemyNum
        {
            get { return limitEnemyNum; }
            set { limitEnemyNum = value; }
        }

        protected override void Awake()
        {

        }

#if UNITY_EDITOR
        private void OnApplicationQuit()
        {

        }
#else
        private void OnApplicationPause(bool pause)
        {

        }
#endif
    }
}
