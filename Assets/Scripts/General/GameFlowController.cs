﻿using System;
using System.Collections;
using UnityEngine;

namespace General
{
    public class GameFlowController : SingletonMonoBehaviour<GameFlowController>
    {
        protected override bool dontDestroyOnLoad { get { return true; } }
        protected ParamBridge pb;
        protected AudioManager am;
        protected InputManager im;

        public IEnumerator timer;
        // public Result result;

        private GameObject[] views;
        public GameObject[] Views
        {
            get { return views; }
            set { views = value; }
        }

        // 画面モード
        [SerializeField] protected ViewMode vmode = ViewMode.Dummy;
        public ViewMode VMode
        {
            get { return vmode; }
            set { vmode = value; }
        }

        // シーンモード
        [SerializeField] protected ScreenMode smode = ScreenMode.Dummy;
        public ScreenMode SMode
        {
            get { return smode; }
            set { smode = value; }
        }

        [SerializeField] protected Signal actionSignal = Signal.Stay;
        public void dispatch(string signal)
        {
            actionSignal = (Signal)Enum.Parse(typeof(Signal), signal, true);
        }

        public void dispatch(Signal signal)
        {
            actionSignal = signal;
        }

        protected override void Init()
        {
            pb = ParamBridge.Instance;
            am = AudioManager.Instance;
            im = InputManager.Instance;
        }

        public virtual void SwitchView(ViewMode next, bool nextActive = false, bool currActive = true)
        {
            Debug.Log($"switching {VMode} to {next}");
            // nextActiveがtrueならSetActiveする
            if (nextActive)
            {
                Array.Find(views, v => v.name == next.ToStringQuickly())?.SetActive(true);
            }
            // currActiveがfalseならSetActiveする
            if (!currActive)
            {
                Array.Find(views, v => v.name == vmode.ToStringQuickly())?.SetActive(false);
            }
            actionSignal = Signal.Stay;
            vmode = next;
        }

        public virtual void SwitchScreenFade(ScreenMode next)
        {
            Debug.Log($"changing to {next.GetEntryViewMode()}");
            actionSignal = Signal.Stay;
            vmode = next.GetEntryViewMode();
            smode = next;
            FadeManager.Instance.LoadScene(next.ToStringQuickly(), 1.0f);
        }
        public virtual void SwitchScreen(ScreenMode next)
        {
            Debug.Log($"changing to {next.GetEntryViewMode()}");
            actionSignal = Signal.Stay;
            vmode = next.GetEntryViewMode();
            smode = next;
            UnityEngine.SceneManagement.SceneManager.LoadScene(next.ToStringQuickly());
        }

        protected override void Update()
        {
            if (actionSignal == Signal.Stay) return;

            switch (vmode)
            {
                // タイトル画面
                case ViewMode.Title:
                    switch (actionSignal)
                    {
                        case Signal.Forward:
                            // レベルビューがほしいとき
                            // SwitchView(ViewMode.LevelList, nextActive: true, currActive: false);
                            SwitchScreenFade(ScreenMode.Game);
                            break;
                        default:
                            Debug.LogError($"Signal {actionSignal} is not allowed.");
                            break;
                    }
                    break;
                // 難易度選択画面
                case ViewMode.LevelList:
                    switch (actionSignal)
                    {
                        case Signal.Forward:
                            SwitchScreenFade(ScreenMode.Game);
                            break;
                        /* シグナル別のやつ作るかTitleManagerにEndGameみたいな制御を組み込むか
                        case Signal.Forward2:
                            break;*/
                        case Signal.Backward:
                            SwitchView(ViewMode.Title, nextActive: true, currActive: false);
                            break;
                    }
                    break;
                // ゲーム画面エントリポイント
                case ViewMode.GameEntry:
                    switch (actionSignal)
                    {
                        case Signal.Forward:
                            SwitchView(ViewMode.InGame, currActive: false);
                            // GameManager.Instance.StartGame();
                            break;
                        default:
                            Debug.LogError($"Signal {actionSignal} is not allowed.");
                            break;
                    }
                    break;
                // ゲーム中画面
                case ViewMode.InGame:
                    switch (actionSignal)
                    {
                        case Signal.Forward:
                            pb.IsOver = true;
                            SwitchView(ViewMode.Result, nextActive: true);
                            break;
                        case Signal.Pause:
                            SwitchView(ViewMode.Pause, nextActive: true);
                            pb.StopTheWorld = true;
                            StopCoroutine(timer);
                            break;
                        case Signal.ToOver:
                            SwitchView(ViewMode.GameOver, nextActive: true);
                            break;
                        default:
                            Debug.LogError($"Signal {actionSignal} is not allowed.");
                            break;
                    }
                    break;
                // ポーズ画面
                case ViewMode.Pause:
                    switch (actionSignal)
                    {
                        case Signal.Backward:
                            SwitchView(ViewMode.InGame, currActive: false);
                            pb.StopTheWorld = false;
                            Time.timeScale = 1f;
                            StartCoroutine(timer);
                            break;
                        case Signal.Restart:
                            SwitchScreenFade(ScreenMode.Game);
                            break;
                        case Signal.ToTitle:
                            SwitchScreenFade(ScreenMode.Start);
                            break;
                        default:
                            Debug.LogError($"Signal {actionSignal} is not allowed.");
                            break;
                    }
                    break;
                // リザルト画面
                case ViewMode.Result:
                    switch (actionSignal)
                    {
                        case Signal.Restart:
                            SwitchScreenFade(ScreenMode.Game);
                            break;
                        case Signal.ToTitle:
                            SwitchScreenFade(ScreenMode.Start);
                            break;
                        default:
                            Debug.LogError($"Signal {actionSignal} is not allowed.");
                            break;
                    }
                    break;
                // ゲームオーバー画面
                case ViewMode.GameOver:
                    switch (actionSignal)
                    {
                        case Signal.Restart:
                            SwitchScreenFade(ScreenMode.Game);
                            break;
                        case Signal.ToTitle:
                            SwitchScreenFade(ScreenMode.Start);
                            break;
                        default:
                            Debug.LogError($"Signal {actionSignal} is not allowed.");
                            break;
                    }
                    break;
                // その他
                default:
                    Debug.LogError($"ViewMode {vmode} is now not supported, so add it to 'GameFlowController.Update()'.");
                    break;
            }
        }


    }



}
