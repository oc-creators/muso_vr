﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace General
{

    public class AudioManager : SingletonMonoBehaviour<AudioManager>
    {
        [SerializeField]
        private AudioSource source;
        public List<AudioClip> bGMClip;
        private float seVolume = 1f;

        protected override bool dontDestroyOnLoad { get { return true; } }

        public AudioSource Source
        {
            get { return source; }
            set { source = value; }
        }

        public float SEVolume
        {
            get { return seVolume; }
            set { seVolume = value; }
        }

        private ParamBridge pb;

        protected override bool CheckInstance()
        {
            if (instance == null)
            {
                instance = this;
                if (dontDestroyOnLoad)
                {
                    DontDestroyOnLoad(this);
                    Debug.Log($"Deployed to DontDestroyOnLoad: {typeof(AudioManager)}");
                }
                return true;
            }
            else if (Instance == this)
            {
                return true;
            }

            /*
            bGMClip.ForEach(clip =>
            {
                if (!bGMClip.Contains(clip))
                {
                    bGMClip.Add(clip);
                }
            });
            */
            Destroy(gameObject);
            return false;
        }

        protected override void Init()
        {
            pb = ParamBridge.Instance;
            // オーディオ管理
            //Debug.Assert(bGMClip[0] != null, $"BGMClip is null");
            //Debug.Assert(source != null);
            source.clip = bGMClip[0];
            source.loop = true;
            //ゲーム上の音量と紐づけする
            Listen();

            Debug.Log("AudioManager: Initialized");
        }

        public void ReflectChanges()
        {
            
        }

        public void Listen()
        {
            
        }

        public void Play()
        {
            Debug.Assert(source != null);
            source.Play();
        }

        /*
        public void Play(string clipName)
        {
            Stop();

            
            var newClip = bGMClip.Find(clip => clip.name == clipName);
            if (newClip != null)
            {
                source.clip = newClip;
                source.Play();
            }
            else
            {
                Debug.LogError($"No such BGM clip `{clipName}'");
            }
            
        }
        */
        public void Play(AudioClip clipName)
        {
            //Stop();

            source.clip = clipName;
            source.Play();
        }


        public void Replay()
        {
            Stop();
            source.Play();
        }

        public void Stop()
        {
            if (source.isPlaying)
            {
                source.Stop();
            }
        }

        public void PlaySE(AudioClip clip)
        {
            if (clip != null)
            {
                source.PlayOneShot(clip, seVolume);
            }
        }
    }
}