﻿using UnityEngine;

namespace General
{
	public enum ViewMode
	{
		// ダミー
		Dummy = -1,
		// Start画面
		Title = 0,
		LevelList,
		GameEntry,
		InGame,
		Pause,
		GameOver,
		Result
	}

	public enum ScreenMode
	{
		Dummy = -1,
		Start,
		Game
	}

	// アクションシグナル
	public enum Signal
	{
		Stay,
		Forward,
		Backward,
		Restart,
		Pause,
		ToTitle,
		ToOver
	}

	public static class ModeHelper
	{
		public static string ToStringQuickly(this ViewMode m)
		{
			switch (m)
			{
				case ViewMode.Title:
					return "StartView";
				case ViewMode.LevelList:
					return "LevelListView";
				case ViewMode.GameEntry:
					return "GameEntryView";
				case ViewMode.InGame:
					return "GameView";
				case ViewMode.Result:
					return "ResultView";
				case ViewMode.GameOver:
					return "GameOverView";
				case ViewMode.Pause:
					return "PauseView";
				case ViewMode.Dummy:
				default:
					Debug.LogWarning($"Configure a case '{m}' of ModeHelper.ToStringQuickly");
					return m.ToString();
			}
		}
		public static string ToStringQuickly(this ScreenMode m)
		{
			switch (m)
			{
				case ScreenMode.Start:
					return "StartScene";
				case ScreenMode.Game:
					return "GameScene";
				case ScreenMode.Dummy:
				default:
					Debug.LogWarning($"Configure a case '{m}' of ModeHelper.ToStringQuickly");
					return m.ToString();
			}
		}

		public static ViewMode GetEntryViewMode(this ScreenMode m)
		{
			switch (m)
			{
				case ScreenMode.Start:
					return ViewMode.Title;
				case ScreenMode.Game:
					return ViewMode.GameEntry;
				case ScreenMode.Dummy:
				default:
					return ViewMode.Dummy;
			}
		}

		public static Signal Parse(string str)
		{
			switch (str)
			{
				case "Forward":
					return Signal.Forward;
				case "Backward":
					return Signal.Backward;
				case "Restart":
					return Signal.Restart;
				case "Pause":
					return Signal.Pause;
				case "ToTitle":
					return Signal.ToTitle;
				case "ToOver":
					return Signal.ToOver;
				default:
					return Signal.Stay;
			}
		}

	}
}