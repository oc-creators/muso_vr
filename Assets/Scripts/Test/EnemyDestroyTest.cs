﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDestroyTest : MonoBehaviour
{
    [SerializeField] [Range(0, 300)] int HP = 100;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void damaged()
    {
        HP -= 100;

        if (HP <= 0)
        {
            Destroy(this.gameObject);
            Debug.Log("destroy");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("PlayerWeapon"))
        {
            damaged();
        }
    }
}
