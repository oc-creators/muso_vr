﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using General;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        // 敵キャラの状態
        public enum State
        {
            Chase,
            Attack,
            Dead 
        }

        private string [] state = { "chase", "attack", "dead" };


        [SerializeField] [Range(0, 300)] int HP = 100;

        // 現在の状態
        private State currState = State.Chase;

        private Animator animator;

        private Rigidbody rb;

        [SerializeField][Range(0f, 50f)] private float forceRate = 10f;

        private NavMeshAgent agent;

        private Vector3 playerPos;

        [SerializeField] [Range(0f, 5f)] private float distance = 0.5f;

        [SerializeField] [Range(0f, 15f)] private float waitTime = 5f;

        [SerializeField] [Range(0f, 15f)] private float deadTime = 5f;

        [SerializeField] private List<AudioClip> seClips;

        [SerializeField] private AudioSource enemyVoice;

        private ParamBridge pb;

        private GameManager gm;

        private AudioManager am;

        // Start is called before the first frame update
        void Start()
        {
            animator = GetComponent<Animator>();
            rb = GetComponent<Rigidbody>();
            agent = GetComponent<NavMeshAgent>();
            playerPos = this.transform.position;
            pb = ParamBridge.Instance;
            gm = GameManager.Instance;
            am = AudioManager.Instance;
            currState = State.Chase;
            enemyVoice = GetComponent<AudioSource>();
            enemyVoice.Play();
        }

        // Update is called once per frame
        void Update()
        {
            DebugPosition();
        }

        private void FixedUpdate()
        {
            Move();
        }

#if DEBUG
        private void OnGUI()
        {
            if (GUILayout.Button("Chase"))
            {
                ChangeAnimation(State.Chase);
            }
            if(GUILayout.Button("Attack"))
            {
                ChangeAnimation(State.Attack);
            }
            if (GUILayout.Button("Dead"))
            {
                attacked();
            }
        }
#endif

        private void Move()
        {
            switch (currState)
            {
                case State.Chase:
                    playerPos = pb.PlayerTR.position;
                    agent.destination = playerPos;// ここは変更
                    if((this.transform.position - playerPos).magnitude < distance)
                    {

                        ChangeAnimation(State.Attack);
                        StartCoroutine(Attacking());
                    }
                    break;
                case State.Attack:
                    break;
                case State.Dead:
                    agent.destination = this.transform.position;
                    break;
            }
        }

        public void attacked()
        {
            Impact();
            pb.DownedEnemyNum++;
            ChangeAnimation(State.Dead);
            StartCoroutine(Dead());
        }

        public void damaged()
        {
            HP -= 100;

            //吹っ飛んで死ぬ
            if(HP <= 0)
            {
                enemyVoice.Stop();
                am.PlaySE(seClips[0]);
                am.PlaySE(seClips[3]);
                Impact();
                pb.DownedEnemyNum++;
                ChangeAnimation(State.Dead);
                StartCoroutine(Dead());
            }
        }

        private void Impact()
        {
            Vector3 forward = this.transform.position;// この後プレイヤーの位置から引き算
            forward -= pb.PlayerTR.position;
            forward.Normalize();
            forward += new Vector3(0, 1, 0);
            rb.AddForce(forward * forceRate, ForceMode.Impulse);
        }

        /* 旧版 Trigger版
        private void ChangeAnimation(State s)
        {
            foreach(string str in state)
            {
                animator.SetBool(str, false);
            }
            string st = ToStringQuickly(s);
            if(s == State.Attack)
            {
                animator.SetTrigger(st);
                animator.SetBool(ToStringQuickly(State.Chase), true);
            } else
            {
                animator.SetBool(st, true);
            }

            currState = s;
        }
        */

        private void ChangeAnimation(State s)
        {
            foreach (string str in state)
            {
                animator.SetBool(str, false);
            }
            string st = ToStringQuickly(s);
            animator.SetBool(st, true);


            currState = s;
        }

        public string ToStringQuickly(State s)
        {
            switch (s)
            {
                case State.Chase:
                    return "chase";
                case State.Attack:
                    return "attack";
                case State.Dead:
                    return "dead";
                default:
                    return s.ToString();
            }
        }

        /*
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Weapon") && currState != State.Dead)
            {
                Impact();// 吹っ飛ぶ
                ChangeAnimation(State.Dead);// やられ姿勢になる
            }
        }
        */

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.CompareTag("PlayerLeftWeapon") && currState != State.Dead)
            {
                damaged();
                StartCoroutine(Vibrate(controller: OVRInput.Controller.LTouch));

                Debug.Log("enemy damaged");
            }
            if (other.gameObject.CompareTag("PlayerRightWeapon") && currState != State.Dead)
            {
                damaged();
                StartCoroutine(Vibrate(controller: OVRInput.Controller.RTouch));
                Debug.Log("enemy damaged");
            }
        }

        public static IEnumerator Vibrate(float duration = 0.2f, float frequency = 0.1f, float amplitude = 0.1f, OVRInput.Controller controller = OVRInput.Controller.Active)
        {
            //コントローラーを振動させる
            OVRInput.SetControllerVibration(frequency, amplitude, controller);

            //指定された時間待つ
            yield return new WaitForSeconds(duration);

            //コントローラーの振動を止める
            OVRInput.SetControllerVibration(0, 0, controller);
        }

        private IEnumerator Attacking()
        {
            am.PlaySE(seClips[2]);

            yield return new WaitForSeconds(waitTime);

            // Debug.Log("Finish waiting...");

            ChangeAnimation(State.Chase);
        }

        private IEnumerator Dead()
        {
            yield return new WaitForSeconds(deadTime);

            gm.SummonEnemy();

            Destroy(this.transform.parent.gameObject);
        }

        private void DebugPosition()
        {
            if(Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;

                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100))
                {
                    playerPos = hit.point;
                    // Debug.Log("MousePosion :" + hit.point);
                }
            }
        }

    }
}
