﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using General;

public class PlayerDamaged : MonoBehaviour
{
    [SerializeField] [Range(0, 10000)] int HP = 10000;
    private ParamBridge pb;
    private GameFlowController gfc;

    // Start is called before the first frame update
    void Start()
    {
        pb = ParamBridge.Instance;
        gfc = GameFlowController.Instance;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void damaged()
    {
        HP -= 100;

        if (HP <= 0)
        {
           // Destroy(this.gameObject);
            Debug.Log("GameOver");
            if (!pb.IsOver)
            {
                pb.IsOver = true;
                gfc.dispatch(Signal.ToOver);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("EnemyWeapon"))
        {
            damaged();
        }
    }
}
