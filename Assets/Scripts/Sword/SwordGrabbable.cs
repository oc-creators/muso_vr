﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OculusSampleFramework;
using General;

public class SwordGrabbable : MonoBehaviour
{
    private DistanceGrabbable m_grabbale;
    private GameManager gm;
    private BoxCollider swordCollider;
    // Start is called before the first frame update
    void Start()
    {
        m_grabbale = GetComponent<DistanceGrabbable>();
        swordCollider = GetComponent<BoxCollider>();
        gm = GameManager.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        if(m_grabbale.isGrabbed)
        {
            swordCollider.isTrigger = true;
            gm.StartGame();
            Debug.Log("grab");
        }
        if(!m_grabbale.isGrabbed)
        {
            swordCollider.isTrigger = false;
            Debug.Log("releace");
        }
    }
}
